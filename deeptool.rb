#! /usr/bin/ruby
#encoding: utf-8

require './lib/deep'

puts "========= DeepGrahics v0.1 =========".yellow

def usage
  puts
  puts "Usage:"
  puts "\tdeepgraphics webserver"
  puts "\tdeepgraphics info <URL>"
  puts "\tdeepgraphics description <URL>"
  puts "\tdeepgraphics response <URL>"
  puts "\tdeepgraphics crawl <URL> levels"
  puts "See:"
  puts "\thttps://gitlab.com/codingrights/deepgraphics"
  puts
end

# parse user arguments
unless ARGV[0]
  usage
else
  case ARGV[0]
    when "info"
      if ARGV[1]
      ap get_info ARGV[1]
    else
      log "which URL?"; usage;
    end

    when "description"
    if ARGV[1]
      ap get_description ARGV[1]
    else
      log "which URL?"; usage;
    end

    when "response"
      if ARGV[1]
      puts get_response_code ARGV[1]
    else
      log "which URL?"; usage;
    end

    when "crawl"
      if ARGV[1] && ARGV[2]
      crawl ARGV[1], ARGV[2].to_i
    else
      log "which URL and level?"; usage;
    end

    when "webserver"
      EventMachine.run do
        require './lib/webserver'
        WebServer.run!
      end

    else
      log "What?"; usage
  end
end