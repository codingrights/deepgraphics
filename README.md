Deep Graphics
=
Wiki https://events.ccc.de/camp/2015/wiki/Session:Deep_Graphics

Tor is not about hide, it is about security.
This tool provides mechanisms to explore & visualize onion services.

Get it running
==
dependencies: git tor ruby bundler postgresql-server libpq-dev

```
git clone https://gitlab.com/codingrights/deepgraphics.git
# install dependencies
bundle install --path vendor/bundle
# setup database
rake db:setup
# run tor instance
tor -f tor.rc &
```

Features
==
Query info about an URL
```
./deeptool.rb info http://zqktlwi4fecvo6ri.onion
```

Get HTTP response code for a URL (200, 302, 404...)
```
./deeptool.rb response http://zqktlwi4fecvo6ri.onion
```

Get onion service description.json
```
./deeptool.rb description http://zqktlwi4fecvo6ri.onion
```

Crawl URL in x levels
```
./deeptool.rb crawl http://zqktlwi4fecvo6ri.onion 2
```

Explore on the browser
```
./deeptool.rb webserver
```
browse http://localhost:2015
