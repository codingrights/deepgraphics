#! /usr/bin/ruby
#encoding: utf-8

require 'pretty_json'
require 'colored'
require 'awesome_print'

def clean_domain url
  uri = URI.parse(url)
  host = uri.host.downcase
  host.start_with?('www.') ? host[4..-1] : host
end

def clean_string str # remove non-UTF-8 chars and extra spaces
  str.encode('UTF-8', :invalid => :replace, :undef => :replace)
  str.delete!("^\u{0000}-\u{007F}")
  str.gsub!(/\n/, '')
  str.gsub!(/\s+/, ' ')
  str.strip
end

def cache! info, html
  url = info[:url]
  domain = clean_domain(url)
  hash = md5(url)
  cached_dir = "cache/#{domain}"
  cached_file = "#{cached_dir}/#{hash}"
  `mkdir #{cached_dir}` unless File.exist?(cached_dir)
  json = PrettyJSON.new info.to_json
  json.to_file("#{cached_file}.info")
  File.open("#{cached_file}.html", 'w') do |file|
    file.puts html
  end
end

def cached_info url
  file = "cache/#{clean_domain(url)}/#{md5(url)}.info"
  if File.exist?(file)
    puts "=> cached #{url}".green
    return JSON.parse(File.read(file))
  end
end

def cached_page url
  file = "cache/#{clean_domain(url)}/#{md5(url)}.html"
  if File.exist?(file)
    return Nokogiri::HTML File.read(file)
  end
end

def description_url url
  uri = URI.parse(url)
  "#{uri.scheme}://#{uri.host}/description.json"
end

def link2url(link, url)
  return '' if !link || link.empty? && !url || url.empty? || link.start_with?("#")
  uri_link = URI.parse(link)
  uri_url = URI.parse(url)
  uri_link.host = uri_url.host unless uri_link.host
  uri_link.scheme = uri_url.scheme unless uri_link.scheme
  uri_link.to_s
end

def md5 str
  Digest::MD5.hexdigest str
end

def datetime
  Time.now.strftime("%d/%m/%Y %H:%M")
end

def log str, error=false
  str = "=> #{str}"
  color = error ? "red" : "yellow"
  puts str.send(color)
end