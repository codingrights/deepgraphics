#! /usr/bin/ruby
#encoding: utf-8

require 'sequel'
require './lib/util'

class Db

  @db = Sequel.connect(
    dbname: 'deepgraphics',
    adapter: 'postgresql',
    user: 'root',
    host: 'localhost'
  )

  def save_onion page
    # exists?
    # new domain?
    # create
    @db[:onion].insert(
      domain: clean_domain(url),
      title: page.title,
      html: page.content,
      created_at: Time.now
    )
    # link
    @db[:links].insert(
      parent_url: parent_url
    )
  end

  def self.setup
    # create initial database structure
    @db.create_table :domains do
      primary_key :id
      String :domain
      String :title
      Integer :count
    end

    @db.create_table :onions do
      primary_key :id
      integer :link_id
      String :md5
      Date :first_visited_at
      Date :last_visited_at
    end

    @db.create_table :links do
      primary_key :id
      Integer :onio_id_from
      Integer :onio_id_to
      Date :first_visited_at
      Date :last_visited_at
    end
  end

end