#! /usr/bin/ruby
#encoding: utf-8

require 'mechanize'
require "socksify"
require 'socksify/http'
require 'net/telnet'
require 'net/http'
require 'digest/md5'
require 'byebug'
require 'timeout'
require 'eventmachine'
require 'ots'
require 'whatlanguage'
require './lib/util'

def get_via_onion url
  # go over TOR
  begin
    page = nil
    start = Time.now
    Timeout::timeout(10) {
      log url
      uri = URI.parse(url)
      uri.path = "/" if uri.path == ""
      Net::HTTP.SOCKSProxy('127.0.0.1', 2017).start(uri.host, uri.port) do |http|
        page = Nokogiri::HTML http.get(uri.path).body
      end
    }
  rescue Timeout::Error
    log 'Timeout! Is TOR listening on port 2017?', true
  else
    puts ("=> in %3.2fs" % [Time.now - start]).green
    page
  end
end

def get_response_code url
  # go over TOR
  begin
    code = nil
    Timeout::timeout(10) {
      log url
      uri = URI.parse(url)
      uri.path = "/" if uri.path == ""
      Net::HTTP.SOCKSProxy('127.0.0.1', 2017).start(uri.host, uri.port) do |http|
        code = http.get(uri.path).code
      end
    }
  rescue Timeout::Error
    log 'Timeout! Is TOR listening on port 2017?', true
  else
    code
  end
end

def get_info url
  if cache = cached_info(url)
    cache
  else
    parse_page get_via_onion(url), url
  end
end

def get_description url
  path = description_url(url)
  response = get_response_code(path)
  return (response == "200" || response == "302") ? get_via_onion(path) : []
end

def parse_page page, url
  return unless page
  html = clean_string(page.to_s)
  uri = URI.parse(url)
  info = {
    title: page.title,
    url: url,
    domain: clean_domain(url),
    scheme: uri.scheme,
    first_seen_at: Time.now
  }
    .merge(read_metadata(page))
    .merge({description: get_description(url)})
  cache! info, html
  info
end

def read_metadata page
  metadata = {}
  if page.to_s.downcase.index('</html>') # html page?
      texts = clean_string page.search('//p').text
      parsed = OTS.parse(texts)
      summary = ''
      parsed.summarize(sentences: 8).each{|s| summary << "#{s[:sentence]} "}
      summary = clean_string(summary)
      wl = WhatLanguage.new(:all)
      metadata = {
        summary: summary,
        keywords: parsed.keywords[0..20],
        language: wl.language(summary),
        md5: md5(page),
      }
    else
      log "=> not valid HTML page", true
    end
end

def crawl url, level
  return unless clean_domain(url).end_with?(".onion")
  if level > -1
    begin
      info = get_info(url)
      page = cached_page(url)
            domain = "http://#{clean_domain(url)}"
      # get all links from page
      links = page.xpath('//a').map { |link| [link.text, link['href']] }
      log "#{links.count} links"
      # for all urls on this url
      links.each do |link|
        title = link[0]
        to_link = link[1]
        if to_link && !to_link.start_with?("#") # anchor
          to_url = link2url(to_link, url)
          log "[#{level}]: #{title} => #{to_url}"
          crawl(to_url, level-1)
        end
      end
    rescue Exception => e
       log e, true
    end
    return page
  end
end

def renew! # tor route
  tor = Net::Telnet::new("Host" => "localhost", "Port" => "2018", "Timeout" => 10, "Prompt" => /250 OK\n/)
  tor.cmd('AUTHENTICATE "entrar"') { |c| print  c == "250 OK\n" ? "Connected to Tor: " : "Cannot authenticate to Tor: "}
  tor.cmd('SIGNAL NEWNYM') { |c| puts c == "250 OK\n" ? "New route!" : "Cannot switch Tor to new route." }
  tor.cmd('GETINFO stream-status') { |c| puts c }
  tor.close
end