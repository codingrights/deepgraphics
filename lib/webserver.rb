#! /usr/bin/ruby
#encoding: utf-8

require 'thin'
require 'slim'
require 'tilt/sass'
require 'tilt/coffee'
require 'sinatra'
require 'em-websocket'
require './lib/db'
require './lib/deep'

class WebServer < Sinatra::Base

  # the thin server runs localy on the CCCamp meeting year
  set :server, 'thin'
  set :static, true
  set :public_dir, File.dirname(__FILE__) + '/views/assets'
  set :threaded, true
  set :sockets, []
  set :port, 2015

  get '/' do
    slim :index
  end

  get '/off' do
    EventMachine.stop
    Process.exit
  end

end

EventMachine::WebSocket.start(:host => '0.0.0.0', :port => 2016) do |ws|
  ws.onmessage do |url|
    # todo: diverse messages
    ws.send get_info(url).to_json
  end
end